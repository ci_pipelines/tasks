# CI/CD Pipelines Workshop

https://gitlab.gwdg.de/ci_pipelines/tasks

## 1. Packaging a Java application

Given the repo for a java application: https://gitlab.gwdg.de/lasslo.steininger/app-projekt-forest-magic

Fork the project. Build jar file (see the repo's instructions) and provide it as an artifact.

Bonus:

- Make 2 different stages for the jar-file and the javadoc documentation
- To make it even more convenient there is a "shortcut" that always points to the newest artifacts: `https://gitlab.gwdg.de/<groupOrUser>/<projectName>/-/jobs/artifacts/master/browse?job=build`
    - with newly created projects `master` might be `main`

Hints:

- You can use an image that already includes java and ant like frekele/ant or use an image where you can install both, e.g. ubuntu:latest. In practice `frekele/ant` would not be optimal since it hasn't been updated for 4 years.

## 2. Gitlab pages

You want to host a website. It need not be complicated. A simple html file with your contact info would be enough:

```html
<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>My personal little website</title>
  <meta name="description" content="A personal website containing only the most basic contact info.">
  <meta name="author" content="TODO">
</head>

<body>
  <h1>My personal website</h1>
  <p>Hi, I am TODO, I study computer science and you can reach me via email: todo@todo.de</p>
</body>
</html>
```

## 3. Your own server
The pre-provided runners are 

You can get servers from the gdwg if you just ask. We will provide servers for this workshop.

1. log in via ssh with the provided passwords
2. add the gitlab runner repository to ubuntu https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner
3. install gitlab runner as described
4. Configure the gitlab runner to your project. Go to settings -> CI/CD to get the registration token. Run `sudo gitlab-runner register` on the server.

Now you can install dependencies and run your project. Let's use https://github.com/huytd/agar.io-clone as an example for an interactive application. We need to:

- `apt install -y nodejs npm`
- install dependencies using `npm`
- 

